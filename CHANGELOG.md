# Changelog
 
### Sistemas Informáticos Industriales - Alejandro López Guerrero

## [v1.5] - 2020-12-22
*Práctica 5*
### Añadido
- Clase *Socket*
- Comunicación entre cliente y servidor por sockets
### Modificado
- Protocolo de recepción y transmisión de datos 
### Elimindado
- Tubería de servidor a cliente
- Tubería de cliente a servidor

## [v1.4] - 2020-12-10
*Práctica 4*
### Añadido
- Programas **cliente** y **servidor**
- Clases *MundoCliente* y *MundoServidor*
- Tubería con nombre de servidor a cliente
- Tubería con nombre de cliente a servidor
- Thread en el servidor
### Modificado
- Comunicación del bot con el cliente
- Comunicación del logger con el servidor
### Eliminado
- Clase *Mundo*
- Programa **tenis**

## [v1.3] - 2020-11-26
*Práctica 3* 
### Añadido
- Funcionalidades **bot** y **logger**
- Implementación del bot controlador de una raqueta
- Comunicación por proyección en memoria
- Implementación del logger para muestreo de puntuación
- Comunicación por tubería con nombre de tenis a logger
### Modificado
- Fichero **tenis** con funcionalidades bot y logger
- Una sola esfera en juego, para simplificación del bot
- Cambios en **.gitignore**


## [v1.1] - 2020-10-22
*Práctica 2* 
### Añadido
- Movimiento de raquetas
- Movimiento de esferas
- Adición de múltiples esferas
- Empleo de plantilla `std::vector`
- Ficheros **README** y **Changelog**

## [v1.0] - 2020-10-21
*Práctica 1*
### Añadido
- Código inicial del proyecto
- Fork del repositorio desde [@arodrifi](https://bitbucket.org/arodrifi)

