#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>

#define MAX 20
#define READ_SIZE 4096

class logger
{
public:
	char name[MAX];
	char datos[READ_SIZE];
	mode_t mode;
	int fdPipe;
};
