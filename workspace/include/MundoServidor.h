// MundoServidor.h: interface for the CMundoServidor class.
//
//////////////////////////////////////////////////////////////////////

#include <vector>
#include "Raqueta.h"
#include <pthread.h>
#include "Socket.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define SIZE 20
#define MAX_SIZE 200
class CMundoServidor
{
public:
	void Init();
	CMundoServidor();
	virtual ~CMundoServidor();

	void InitGL();
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();
	void RecibeComandosJugador();

	//Atributos
	std::vector<Esfera> listaEsferas;
	std::vector<Plano> paredes;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;

	int puntos1;
	int puntos2;

	//Atributos FIFO LOGGER
	char nameFIFOLogger[SIZE];
	int fdPipeLogger;
	char mensajeLogger[MAX_SIZE];

	pthread_t thid;//Identificador del thread

	Socket conexion,comunicacion;
};
