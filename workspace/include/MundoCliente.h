// MundoCliente.h: interface for the CMundoCliente class.
//
//////////////////////////////////////////////////////////////////////

#include <vector>
#include "Plano.h"
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "DatosMemCompartida.h"
#include "Socket.h"

#define SIZE 20
#define MAX_SIZE 200
class CMundoCliente
{
public:
	void Init();
	CMundoCliente();
	virtual ~CMundoCliente();	
	
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();	

	std::vector<Esfera> listaEsferas;
	std::vector<Plano> paredes;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;

	int puntos1;
	int puntos2;

	//Proyección en memoria
	int fd;
	DatosMemCompartida datosCompartidos;
	DatosMemCompartida* pdatosCompartidos;

	//Sockets
	Socket comunicacion;

};
