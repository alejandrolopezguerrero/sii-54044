#include "logger.h"
#include <string.h>

#define READ_SIZE 4096

int main(int argc,char* argv[])
{
	logger milogger;
 	strcpy(milogger.name,"TuberiaLogger");
        milogger.mode=0666;

	//Crear Tubería
        if(mkfifo(milogger.name, milogger.mode)!=0){
                perror("mkfifo: ");
	        return 1;

        }

	//Abrir Tubería
        milogger.fdPipe=open(milogger.name, O_RDONLY);
        if(milogger.fdPipe==-1){
                perror("Open: ");
        	return 1;
        }

	//Leer Tubería
        while(read(milogger.fdPipe,milogger.datos,READ_SIZE)>0){
                write(1,milogger.datos,strlen(milogger.datos));
        }

	//Cerrar y desvincular Tubería
        if(close(milogger.fdPipe)<0){
                perror("close: ");
                return 1;
        }
        if(unlink(milogger.name)<0){
                perror("unlink: ");
                return 1;
        }
        return 0;
}
