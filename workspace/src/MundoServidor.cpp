// Autor: Alejandro López Guerrero
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoServidor.h"

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include "glut.h"


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

void* hilo_comandos(void* d);

CMundoServidor::CMundoServidor()
{
	Init();
}

CMundoServidor::~CMundoServidor()
{
	if(close(fdPipeLogger)<0){
                perror("close logger: ");
        }
}

void CMundoServidor::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);

	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );

	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundoServidor::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i, j;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	for(j=0;j<listaEsferas.size();j++)
        	listaEsferas[j].Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundoServidor::OnTimer(int value)
{
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);

	int i, j;

	for(j=0;j<listaEsferas.size();j++)
        	listaEsferas[j].Mueve(0.025f);


	for(i=0;i<paredes.size();i++)
	{
		for(j=0;j<listaEsferas.size();j++)
             		paredes[i].Rebota(listaEsferas[j]);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}
	for(j=0;j<listaEsferas.size();j++){
		jugador1.Rebota(listaEsferas[j]);
		jugador2.Rebota(listaEsferas[j]);

		if(fondo_izq.Rebota(listaEsferas[j]))
		{
			listaEsferas[j].centro.x=0;
			listaEsferas[j].centro.y=rand()/(float)RAND_MAX;
			listaEsferas[j].velocidad.x=2+2*rand()/(float)RAND_MAX;
			listaEsferas[j].velocidad.y=2+2*rand()/(float)RAND_MAX;
			puntos2++;
			sprintf(mensajeLogger,"Jugador 2 ha anotado. Tiene %d puntos\n",puntos2);
			write(fdPipeLogger,mensajeLogger,strlen(mensajeLogger));
		}

		if(fondo_dcho.Rebota(listaEsferas[j]))
		{
			listaEsferas[j].centro.x=0;
			listaEsferas[j].centro.y=rand()/(float)RAND_MAX;
			listaEsferas[j].velocidad.x=-2-2*rand()/(float)RAND_MAX;
			listaEsferas[j].velocidad.y=-2-2*rand()/(float)RAND_MAX;
			puntos1++;
			sprintf(mensajeLogger,"Jugador 1 ha anotado. Tiene %d puntos\n",puntos1);
			write(fdPipeLogger,mensajeLogger,strlen(mensajeLogger));
		}
	}

	char cad[200];
	sprintf(cad,"%f %f %f %f %f %f %f %f %f %f %d %d", listaEsferas[0].centro.x,listaEsferas[0].centro.y, jugador1.x1,jugador1.y1,jugador1.x2,jugador1.y2, jugador2.x1,jugador2.y1,jugador2.x2,jugador2.y2, puntos1, puntos2);
	comunicacion.Send(cad,sizeof(cad));

	if(puntos1==3||puntos2==3){

	exit(0);
	}

}

void CMundoServidor::OnKeyboardDown(unsigned char key, int x, int y)
{
/*
	switch(key)
	{
	case 's':jugador1.velocidad.y=-4;break;
	case 'w':jugador1.velocidad.y=4;break;
	case 'l':jugador2.velocidad.y=-4;break;
	case 'o':jugador2.velocidad.y=4;break;

	}
*/
}

void CMundoServidor::Init()
{

//Inicialización del Socket
	char ip[]="127.0.0.1";
	if(conexion.InitServer(ip,8000)==-1)
	        fprintf(stderr,"Error InitServer: ");
	comunicacion=conexion.Accept();
	char nombre[100];
	comunicacion.Receive(nombre,sizeof(nombre));
	printf("%s se ha unido a la partida\n",nombre);


//Tubería logger
	strcpy(nameFIFOLogger,"TuberiaLogger");
	fdPipeLogger=open(nameFIFOLogger, O_WRONLY);
	fprintf(stderr,"Tubería logger abierta\n");

	pthread_create(&thid, NULL, hilo_comandos, this);

	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;

//esferas
	Esfera e1;//añadir e2 para múltiples esferas, y descomentar pushback;
	listaEsferas.push_back(e1);

	//listaEsferas.pushback(e2);
}

void* hilo_comandos(void* d)
{
      CMundoServidor* p=(CMundoServidor*) d;
      p->RecibeComandosJugador();
}

void CMundoServidor::RecibeComandosJugador(){
	while (1) {
		usleep(10);
		char cad[100];
                comunicacion.Receive(cad,sizeof(cad));
            	unsigned char key;
            	sscanf(cad,"%c",&key);
            	if(key=='s')jugador1.velocidad.y=-4;
            	if(key=='w')jugador1.velocidad.y=4;
            	if(key=='l')jugador2.velocidad.y=-4;
            	if(key=='o')jugador2.velocidad.y=4;
      }
}
