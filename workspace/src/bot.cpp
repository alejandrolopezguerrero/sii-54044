#include "DatosMemCompartida.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>

int main(int argc,char* argv[]){

	DatosMemCompartida* pdatosCompartidos;
	int fd;
	struct stat *statbuf;

	fd=open("Memoria", O_RDWR);
	fstat(fd, statbuf);

	pdatosCompartidos=static_cast<DatosMemCompartida*>(mmap(NULL,statbuf->st_size, PROT_READ|PROT_WRITE,MAP_SHARED,fd,0));
	close(fd);

	while(1){
		if(pdatosCompartidos->esfera.centro.y>pdatosCompartidos->raqueta1.y1)
			pdatosCompartidos->accion=1;
		else if(pdatosCompartidos->esfera.centro.y<pdatosCompartidos->raqueta1.y2)
			pdatosCompartidos->accion=-1;
		else
			pdatosCompartidos->accion=0;

		usleep(25000);
	}

}

